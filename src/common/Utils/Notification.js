import { ToastContainer,toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

     function def (mes) {
        toast(mes)
    }
    
     function notifySuccess (mes) {
        toast.success(mes)
    }
    
     function notifyError (mes) {
        toast.error(mes)
    }
    
    const notifyWarning= () => {toast.warning()}
    
     function notifyInfo (mes) {
        toast.info(mes)
    }
    


export {notifySuccess, notifyError, notifyWarning, notifyInfo}