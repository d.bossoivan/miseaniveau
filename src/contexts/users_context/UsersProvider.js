import { useContext, useReducer } from "react"
import UsersContext from "./UsersContext"

function UsersContextReducer(state, action){
    switch(action.name){

        case "CREATE_USER":{
            return{...state, users:action.data}
        }
        default:{
            throw new Error(`UsersContext does not impliment the ${action.name} case`)
        }
    }
}

const UsersProvider = ({children}) => {
    let initialState={
       users:[{ userId: "001",
                lastLogIn: new Date(),
                lastLogOut: new Date(),
                nom: "Ngatchou Christian",
                email: "Christian@gmail.com",
                mdp:"chris",
                isConnect:false
        },
        { userId: "002",
                lastLogIn: new Date(),
                lastLogOut: new Date("09/12/2020"),
                nom: "Marco Kuidja",
                email: "anabelle@gmail.com",
                mdp:"chris",
                isConnect:false
        },
        { userId: "003",
                lastLogIn: new Date(),
                lastLogOut: new Date("03/09/2021"),
                nom: "Yempo Franck",
                email: "Infinityn@gmail.com",
                mdp:"chris",
                isConnect:false
        },
        { userId: "004",
                lastLogIn: new Date(),
                lastLogOut: new Date("02/03/2021"),
                nom: "The Lord",
                email: "Avengers77@gmail.com",
                mdp:"chris",
                isConnect:false
        },
        { userId: "005",
                lastLogIn: new Date(),
                lastLogOut: new Date("03/05/2021"),
                nom: "Nolo toto Machep",
                email: "Christian@gmail.com",
                mdp:"chris",
                isConnect:false
        }]
    }

    let [usersState,usersDispatch] = useReducer(UsersContextReducer, initialState)
    let value={usersState, usersDispatch}

    return (
        <UsersContext.Provider value={value}>
            {children}
        </UsersContext.Provider>
    );
}

export default UsersProvider;

const useUsers=()=>{
    let context = useContext(UsersContext)
    if(context === undefined){
        throw new Error ("useMode must be used between a ModeProvider tag")
    }else return context

}
export{useUsers}