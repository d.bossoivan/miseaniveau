import { useContext, useReducer } from "react";
import ModeContext from "./ModeContext";

const ModeContextReducer = (state, action)=>{
    switch(action.type){
        case "SET_DARK_THEME":{
            return {...state, localMode: "dark"}
        }

        case "SET_LIGHT_Mode":{
            return {...state, localMode: "light"}
        }
        
        default: {
            throw new Error(`ModeContext does not implement the ${action.type} case`)
        }
    }
}

const ModeProvider = ({children}) => {
    const initialState = {
        localMode:"light" 
    }
    
    const [ModeState, ModeDispatch] = useReducer(ModeContextReducer, initialState)
    const value = {ModeState, ModeDispatch};

    return ( 
        <ModeContext.Provider value ={value} >
            {children}
        </ModeContext.Provider>
     );
}
export default ModeProvider;

const useMode = () =>{
    let context = useContext(ModeContext)
    if(context === undefined){
        throw new Error ("useMode must be used between a ModeProvider tag")
    }else return context
}

export { useMode }
