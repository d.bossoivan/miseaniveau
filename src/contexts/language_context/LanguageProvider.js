import { useContext, useReducer } from "react";
import LanguageContext from "./LanguageContext";

const LanguageContextReducer = (state, action)=>{
    switch(action.type){
        case "SET_EN_LANG":{
            localStorage.setItem("textMeLang", "en")
            return {...state, localLang: "en"}
        }

        case "SET_FR_LANG":{
            localStorage.setItem("textMeLang", "fr")
            return {...state, localLang: "fr"}
        }
        
        default: {
            throw new Error(`LanguageContext does not implement the ${action.type} case`)
        }
    }
}

const LanguageProvider = ({children}) => {
    const initialState = {
        localLang: localStorage.getItem("textMeLang") || 'fr'
    }
    
    const [languageState, languageDispatch] = useReducer(LanguageContextReducer, initialState)
    const value = {languageState, languageDispatch};

    return ( 
        <LanguageContext.Provider value ={value} >
            {children}
        </LanguageContext.Provider>
     );
}
 
export default LanguageProvider;

const useLanguage = () =>{
    let context = useContext(LanguageContext)
    if(context === undefined){
        throw new Error ("useLanguage must be used between a LanguageProvider tag")
    }else return context
}

export { useLanguage }
