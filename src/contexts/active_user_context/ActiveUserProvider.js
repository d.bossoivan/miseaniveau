import ActiveUserContext from "./ActiveUserContext";
import { useContext, useReducer } from "react";

function ActiveUserContextReducer(state, method) {
    switch(method.name){
        case "SET_ACTIVE_USER": {
            return {...state, activeUser: method.data }
        }
        default:{
            throw new Error(`ActiveUserContext does not implement the ${method.name} case`)
        }
    }
}


const ActiveUserProvider = ({children}) => {
    let initialState ={
        activeUser:{}
    }
    let [activeUserState, activeUserDispatch] = useReducer(ActiveUserContextReducer, initialState)
    let value ={activeUserState, activeUserDispatch}
    return ( 
        <ActiveUserContext.Provider value={value} >
            {children}
        </ActiveUserContext.Provider>
     );
} 
export default ActiveUserProvider;

function useActiveUser(){
    let context = useContext(ActiveUserContext)
    if(context === undefined){
        throw new Error("useActiveUser must be used between an activeUserProvider tag")
    }else return context;
}

export {useActiveUser}
