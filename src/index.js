import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import LanguageProvider from './contexts/language_context/LanguageProvider';
import ModeProvider from './contexts/mode_context/ModeProvider';
import { BrowserRouter as Router } from 'react-router-dom';

ReactDOM.render(
<Router>
  <ModeProvider>
    <LanguageProvider>
      <App />
    </LanguageProvider>
  </ModeProvider>
</Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
