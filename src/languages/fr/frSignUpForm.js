
const frSignUpForm =  {
    header:"Inscrivez-Vous !!!",
    nameField:"Nom complet",
    email:"Email",
    passwordField:"Mot de passe",
    confirmpasswordField:"Confirmez le mot de passe",
    iHaveanaccount:"J'ai deja un compte",
    submitbutton:"S'inscrire", 

    nameRequiredError: "Entrez un nom",
    confirmPasswordRequiredError: "Retapez le mot de passe",
    notIdenticalPassword: "Les mots de passe ne correspondent pas",
    nameInput: "Noms complets",
    confirmPasswordLabel: "Confirmez le mot de passe",
    iHaveAnAccount: "Vous avez déjà un compte ?",
    signUpButton: "S'incrire",

    userListHeader: "Liste des Utilisateurs",
    discussionHeader: "Discussion",
    menuTooltip: "Menu",

    usedEmailError:"Cet Email est deja utilisé"

}
 
export default frSignUpForm;