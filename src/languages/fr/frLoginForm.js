
const frLoginForm =  {
    headerLogin:"Connectez-vous!!!",
    email:"Email",
    passwordField:"Mot de passe",
    iHaveNoaccount:"je n'ai pas de compte",
    submitbutton:"Se connecter",

    emailRequiredError: "e-mail est obligatoire",
    invalidEmailError: "e-mail non valide",
    passwordRequiredError: "mot de passe est requis",
    emailTextfieldLabel: "Email",
    passwordTextfieldLabel: "Mot de Passe",
    signInButton: "Se Connecter",
    visibilityOff: "Afficher mot de passe",
    visibility: "Masquer mot de passe",
    iDontHaveAccount: "Je n'ai pas de compte",
    unknownUserError:"Email ou Mot de passe incorrect"

}
 
export default frLoginForm;