import { defineMessages } from "react-intl";
import frSignUpForm from "./frSignUpForm";
import frLoginForm from './frLoginForm';
const frMessages =  {
    ...frSignUpForm,
    ...frLoginForm
}
    
 
export default defineMessages( frMessages);