const enLoginForm =  {
    headerLogin:"Log In !!!",
    email:"Email",
    passwordField:"Password",
    iHaveNoaccount:"I don't have an account",
    submitbutton:"Login",
    emailRequiredError: "Email is complusory",
    invalidEmailError: "Invalid email",
    passwordRequiredError: "Password is required",
    emailTextfieldLabel: "Email",
    passwordTextfieldLabel: "Password",
    signInButton: "Sign in",
    visibilityOff: "Show Password",
    visibility: "Hide Password",
    iDontHaveAccount: "I don't have an account",

    userListHeader: "Users list",
    discussionHeader: "User Thread",
    menuTooltip: "Menu",

    unknownUserError:"Incorrect Email or Password",


}
 
export default enLoginForm;