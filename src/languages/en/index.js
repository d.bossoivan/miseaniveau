import { defineMessages } from "react-intl";
import enLoginForm from "./enLoginForm";
import enSignUpForm from "./enSignUpForm";

const enMessages =  {
    ...enSignUpForm,
    ...enLoginForm
    
}
    
 
export default defineMessages( enMessages);