const enSignUpForm =  {
    header:"Sign Up !!!",
    nameField:"Full Name",
    email:"Email",
    passwordField:"Password",
    confirmpasswordField:"Confirm the Password",
    iHaveanaccount:"I have an account",
    submitbutton:"Sign up", 

    nameRequiredError: "Enter a name",
    confirmPasswordRequiredError: "Re-enter password",
    notIdenticalPassword: "Passwords do not match",
    nameInput: "Full Names",
    confirmPasswordLabel: "Confirm Password",
    iHaveAnAccount: "Already have an account?",
    signUpButton: "Create Account",

    searchPlaceholder: "Search Contact",

    usedEmailError:"This email is already used",

}
 
export default enSignUpForm;