import { Navigate } from "react-router";
import LoginForm from "./components/log_in_form/LoginForm";
import SignUpForm from "./components/sign_up_form/SignUpForm";

const routes =[
    {
        path: "/login" ,
        element: <LoginForm/>
    },
    {
        path:"/signUp",
        element: <SignUpForm/>,
    },
    {
        path:"/",
        element:<SignUpForm/>
    },
    // {
    //     path:"*",
    //     element: <Navigate to="/404"/>
    // }
]

export default routes;
