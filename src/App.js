import { IntlProvider } from "react-intl";
import {ThemeProvider} from '@material-ui/styles'
import {useRoutes} from 'react-router-dom';
import routes from './routes';
import { Box, makeStyles, Typography } from '@material-ui/core';
import logo from './assests/Images/pic.png';
import './App.css';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import frMessages from './languages/fr/index';
import enMessages from './languages/en/index';
import { useLanguage } from './contexts/language_context/LanguageProvider';
import  {useMode} from'./contexts/mode_context/ModeProvider';
import TestComponent from "./components/TestComponent";
import DarkTheme,  {LightTheme}  from "./ui/theme";
import UsersProvider from "./contexts/users_context/UsersProvider";
import ActiveUserProvider, { useActiveUser } from "./contexts/active_user_context/ActiveUserProvider";

const useStyles = makeStyles(theme => ({
  logo_form_box:{
    display:"grid",
    gridAutoFlow:"column"

  }
}))
function App() {
  const routing = useRoutes(routes);
  const {languageState} = useLanguage();
  const {ModeState} = useMode();
  const {localLang} = languageState
  const {localMode} = ModeState
  const classes = useStyles();
  let messages= localLang ==="fr" ? frMessages:enMessages

  
  return (
      <IntlProvider locale={localLang} messages={messages}>
        <UsersProvider>
          <ActiveUserProvider>
            <ThemeProvider theme={localMode ==="light" ? LightTheme:DarkTheme} >
              {/* <TestComponent/> */}
              <ToastContainer/>
              <Typography> The Mode is : {localMode }</Typography>
                <Box className="App">
                  <Box className={classes.logo_form_box}>
                    <Box className="imageBox">
                      <img src={logo} alt="TextMe"/>
                    </Box>
                    {routing}
                  </Box>
                </Box>
            </ThemeProvider>
          </ActiveUserProvider>
        </UsersProvider>
      </IntlProvider>
  );
}

export default App;
