// import { Box,Button, makeStyles } from "@material-ui/core";
// import { ToastContainer } from "react-toastify";
// import 'react-toastify/dist/ReactToastify.css';
// import notifications from "../common/Utils/Notification";
// import { useMode } from "../contexts/mode_context/ModeProvider";


// const useStyles = makeStyles((theme) => ({
//     root: {
//       '& > *': {
//         margin: theme.spacing(1),
//       },
//     },
//   }));
// const TestComponent = () => {

//      const classes = useStyles()
//     let {ModeState, ModeDispatch} = useMode();
//     const changeTheme = () => {
//         console.log("From: ",ModeState)
//         ModeDispatch({type:"SET_DARK_THEME"})
//         console.log("To: ", ModeState)
//     }


//     return ( 
//         <Box className={classes.root} >
//             <Button size="large"
//                 variant="contained"
//                 color="primary" 
//                 onClick={notifications.default}>
//                DEFAULT
//             </Button>
//             <Button size="large"
//                 variant="outlined"
//                 color="primary" 
//                 onClick={notifications.sucess}>
//                 SUCCESS
//             </Button>
//             <Button size="larger"
//                 variant="contained"
//                 color="primary" 
//                 onClick={notifications.warning}>
//                 Warning
//             </Button>
//             <Button size="large"
//                 variant="outlined"
//                 color="primary" 
//                 onClick={notifications.info}>
//                 INFO
//             </Button>
//             <ToastContainer/>
//         </Box>
//      );
// }
 
// export default TestComponent;