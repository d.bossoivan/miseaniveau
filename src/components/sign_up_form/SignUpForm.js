import { Box, CircularProgress, Grid, TextField, makeStyles, Typography, Button, Tooltip, IconButton } from '@material-ui/core';
import { useFormik } from 'formik'; 
import * as Yup from 'yup';
import { useActiveUser } from '../../contexts/active_user_context/ActiveUserProvider';
import { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { useUsers } from './../../contexts/users_context/UsersProvider';
import { Link, useNavigate } from 'react-router-dom';
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Visibility from '@material-ui/icons/Visibility';
import { signUp } from '../../services/authentication.services';
import {notifyError} from '../../common/Utils/Notification';


const useStyles = makeStyles(theme => ({
    
    field: {
        backgroundColor: theme.palette.secondary.main
    },
    
    form :{
        display:"grid",
        gridAutoFlow:"row",
        gridGap:"10px",
        margin:"10px"
    },

    box:{
        display:"grid",
        gridAutoFlow: "column",
        backgroundColor:"#ffff",
        padding:"10px",
        width:"60vw",
    },
    formFooter:{
        display: 'grid',
        gridAutoFlow: 'column',
        marginTop:"10px"
    },
    formFooterLink:{
        textDecoration:"none",
        '&hover, &active, &visited':{
            color:theme.palette.primary.main
        }
    }
}))


function SignUpForm({intl}) { 
    let navigate= useNavigate();
    let { activeUserState, activeUserDispatch } = useActiveUser();
    let {usersState, usersDispatch} = useUsers();
    let { activeUser} = activeUserState;
    let {users} = usersState;
    let {formatMessage} = intl;
    const [isLoading, setIsLoading] = useState(false);
    const [isPasswordVisible, setIsPasswordVisible] = useState(false)
    
    const initialValues =  {
        nom:'',
        email:'',
        mdp:'',
        cmdp:''
    }
    const onSubmit =(values) => {
        setIsLoading(true)
        let userInfo ={
            userId: "ee7671db-a2ea-4925-a29c-4fed0b1f21fb",
            lastLogIn:new Date(),
            lastLogOut: new Date(),
            isConnect:false,
            ...values
        }
        users.push(userInfo)
        const signup_response = signUp(values.email)
        
         setTimeout(() => {
             if(signup_response.is_signed_in){

                 usersDispatch({name:"CREATE_USER", data:users})
                 //set as activeUser user who just created account
                 activeUserDispatch({ name:"SET_ACTIVE_USER", data: userInfo })
                 setIsLoading(false)
                 //navigate('/')
                 //navigate('login')
             }else {
                 notifyError(formatMessage({ id:"usedEmailError"}))
                 setIsLoading(false)
             }
         },1000);       
    }
    const validationSchema = Yup.object({
        nom: Yup.string().required(formatMessage({ id:"nameRequiredError" })),
        email: Yup.string()
            .required(formatMessage({ id:"emailRequiredError" }))
            .email(formatMessage({ id:"invalidEmailError" })),
        mdp: Yup.string().required(formatMessage({ id:"passwordRequiredError" })),
        cmdp: Yup
            .string()
            .required(formatMessage({ id:"confirmPasswordRequiredError" }))
            .oneOf([Yup.ref("mdp"),null], formatMessage({ id:"notIdenticalPassword" }))
    
    })
    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })
    const classes = useStyles()
     useEffect(()=> {
        console.log('Form Values', activeUser)
         console.log('All Users', usersState)
     }, [ users])

    return (  
        <Box boxShadow={10} className={classes.box}>
            <Grid container>   
                <Grid item xs ={12} sm ={12} md={12}  >
                    <Typography 
                        variant= "h4"
                        align="center"
                        color="primary"
                    >
                        {formatMessage({id:"header"})} 
                    </Typography>
                        <form  onSubmit={formik.handleSubmit} className={classes.form}>
                            <TextField
                                id="nom"
                                className={classes.field}
                                label={formatMessage({ id:"nameInput" })}
                                variant= "outlined"
                                color="secondary"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.nom && formik.errors.nom}
                                value={formik.values.nom}
                                helperText={formik.touched.nom && formik.errors.nom} 
                                fullWidth
                            />
                            <TextField
                                id="email"
                                className={classes.field}
                                label={formatMessage({ id:"emailTextfieldLabel" })}
                                type="email"
                                variant= "outlined"
                                color="secondary"
                                {...formik.getFieldProps('email')}
                                error={formik.touched.email && formik.errors.email}
                                helperText={formik.touched.email && formik.errors.email}                    
                                fullWidth
                            />
                            <TextField
                                id="mdp"
                                className={classes.field}
                                label={formatMessage({ id:"passwordTextfieldLabel" })}
                                variant= "outlined"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.mdp && formik.errors.mdp}
                                value={formik.values.mdp}
                                helperText={formik.touched.mdp && formik.errors.mdp} 
                                color="secondary"
                                fullWidth
                                type={ isPasswordVisible? "text" : "password" }
                                InputProps={{
                                    endAdornment: (
                                    <Tooltip arrow title={ isPasswordVisible? formatMessage({ id:"visibility" }) : formatMessage({ id:"visibilityOff" })}>
                                      <IconButton onClick={()=>setIsPasswordVisible(!isPasswordVisible)}>
                                        {isPasswordVisible? <Visibility /> : <VisibilityOff />}
                                      </IconButton>
                                    </Tooltip>
                                )}}
                            />
                            <TextField
                                id="cmdp"
                                className={classes.field}
                                label={formatMessage({ id:"confirmPasswordLabel" })}
                                variant= "outlined"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.cmdp && formik.errors.cmdp}
                                value={formik.values.cmdp}
                                helperText={formik.touched.cmdp && formik.errors.cmdp} 
                                color="secondary"
                                fullWidth
                                type={ isPasswordVisible? "text" : "password" }
                                InputProps={{
                                    endAdornment: (
                                    <Tooltip arrow title={ isPasswordVisible? formatMessage({ id:"visibility" }) : formatMessage({ id:"visibilityOff" })}>
                                      <IconButton onClick={()=>setIsPasswordVisible(!isPasswordVisible)}>
                                        {isPasswordVisible? <Visibility /> : <VisibilityOff />}
                                      </IconButton>
                                    </Tooltip>
                                )}}
                            />
                            <div className={classes.formFooter}>
                                <Typography
                                    varaiant = "h6"
                                    align="left"
                                    color="primary"
                                    display="inline"
                                    style={{alignSelf: "center"}}
                                >
                                   <Link to="/login" className={classes.formFooterLink}>{formatMessage({ id:"iHaveAnAccount" })}</Link> 
                                </Typography>
                                <Button
                                    variant= "outlined"
                                    color="primary"
                                    type="submit"
                                    align="right"
                                    style={{justifySelf:"right"}}
                                >
                                    {formatMessage({ id:"signUpButton" })}
                                    {isLoading &&
                                    <CircularProgress style={{marginInline:"15px", width:"20px", height:"20px" }}/>
                                    }
                                </Button>   
                            </div>
                        </form>
                </Grid>
            </Grid>
        </Box> 
     );
}
 
export default injectIntl(SignUpForm);