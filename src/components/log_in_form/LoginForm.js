
import Button from  '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { makeStyles} from '@material-ui/core/styles';
import { Box, CircularProgress, Grid, TextField, Tooltip, IconButton } from '@material-ui/core';
import {Formik, useFormik} from 'formik'; 
import * as Yup from 'yup';
import { useActiveUser } from '../../contexts/active_user_context/ActiveUserProvider';
import { useEffect, useState } from 'react';
import { injectIntl } from 'react-intl';
import { useUsers } from '../../contexts//users_context/UsersProvider';
import { Link, useNavigate } from 'react-router-dom';
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Visibility from '@material-ui/icons/Visibility';
import login from '../../services/authentication.services';
import {notifyError} from '../../common/Utils/Notification';
import { ToastContainer } from 'react-toastify';


const useStyles = makeStyles(theme => ({
    box:{
        display:"grid",
        gridAutoFlow: "column",
        backgroundColor:"#ffff",
        padding:"10px",
        width:"60vw",
    },
    field: {
        // marginTop:20,
        // marginBottom:20,
        // display: 'block',
        backgroundColor: theme.palette.secondary.main  
    },
    form :{
        display:"grid",
        gridAutoFlow:"row",
        gridGap:"10px",
        margin:"10px"
    },
    formFooter:{
        display: 'grid',
        gridAutoFlow: 'column',
        marginTop:"10px"
    }

}))

function LoginForm ({intl}) { 
    let navigate = useNavigate();
    let { activeUserState, activeUserDispatch } = useActiveUser();
    let { activeUser} = activeUserState;
    let {usersState, usersDispatch} = useUsers();
    let {users} = usersState;
    let {formatMessage} = intl;
    const [isLoginFormSubmitting, setIsLoginFormSubmitting] = useState(false);

    const initialValues =  {
        email:'',
        mdp:''   
    }
    const onSubmit = (values) => {
        setIsLoginFormSubmitting(true)
        const login_response  =  login(values.email, values.mdp)
        setTimeout(() => {
            if (login_response.is_logged_in){
                activeUserDispatch({name:"SET_ACTIVE_USER",data:values})
                setIsLoginFormSubmitting(false)
                console.log("OK")
                navigate('/login')
            } else {
                console.log("Unkown User")
                notifyError(formatMessage({id:"unknownUserError"}))
                setIsLoginFormSubmitting(false)
            }
        }, 2000);
            
    }

    const validationSchema = Yup.object({
        email: Yup.string()
            .required(formatMessage({ id:"emailRequiredError" }))
            .email(formatMessage({ id:"invalidEmailError" })),
        mdp: Yup.string().required(formatMessage({ id:"passwordRequiredError" })),
    
    })
    const formik = useFormik({
        initialValues,
        onSubmit,
        validationSchema
    })

    const classes = useStyles()

    useEffect(()=> {
        console.log('Form Values', activeUser)
        console.log('All Users', users)

    }, [ users])
    const [isPasswordVisible, setIsPasswordVisible] = useState(false)

    return (  
        <Box boxShadow={10} className={classes.box}>
            <Grid container>   
                <Grid item xs ={12} sm ={12} md={12}  >
                    <Typography 
                        variant= "h4"
                        align="center"
                        color="primary"
                    >
                        {formatMessage({id:"headerLogin"})}
                    </Typography>
                    <Formik>
                    <form  onSubmit={formik.handleSubmit} className={classes.form}>
                            <TextField
                                id="email"
                                className={classes.field}
                                label={formatMessage({ id:"emailTextfieldLabel" })}
                                type="email"
                                variant= "outlined"
                                color="secondary"
                                {...formik.getFieldProps('email')}
                                error={formik.touched.email && formik.errors.email}
                                helperText={formik.touched.email && formik.errors.email}                    
                                fullWidth
                            />
                            
                            <TextField
                                id="mdp"
                                className={classes.field}
                                label={formatMessage({ id:"passwordTextfieldLabel" })}
                                variant= "outlined"
                                type={ isPasswordVisible? "text" : "password" }
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.mdp && formik.errors.mdp}
                                value={formik.values.mdp}
                                helperText={formik.touched.mdp && formik.errors.mdp} 
                                color="secondary"
                                fullWidth
                                InputProps={{
                                    endAdornment: (
                                    <Tooltip arrow title={ isPasswordVisible? formatMessage({ id:"visibility" }) : formatMessage({ id:"visibilityOff" })}>
                                      <IconButton onClick={()=>setIsPasswordVisible(!isPasswordVisible)}>
                                        {isPasswordVisible? <Visibility /> : <VisibilityOff />}
                                      </IconButton>
                                    </Tooltip>
                                )}}
                            />
                     
                            <Box className={classes.formFooter}>
                                <Typography
                                    varaiant = "h6"
                                    align="left"
                                    color="primary"
                                    display="inline"
                                    style={{ alignSelf: "center" }}
                                >
                                       <Link to="/SignUp"> { formatMessage({ id:"iDontHaveAccount" }) } </Link> 
                                </Typography>
                                <Button
                                    variant= "outlined"
                                    color="primary"
                                    type="submit"
                                    align="right"
                                    style={{ justifySelf:"right" }}
                                >
                                    {formatMessage({ id: "signInButton" })}
                                    {isLoginFormSubmitting &&
                                    <CircularProgress style={{ marginInline: "15px", width: "20px", height:  "20px" }}/>
                                    }
                                </Button>   
                                
                            </Box>
                        </form>
                    </Formik>
                </Grid>
            </Grid>
        </Box> 
     );
}
 
export default injectIntl(LoginForm);