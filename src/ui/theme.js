import { createTheme } from "@material-ui/core";

const LightTheme = createTheme({
    palette:{
        type:"light",
        primary:{
            main:"#1b8adb"
        },
        secondary:{
            main:"#ebf2f2"
        }

    }
})
export {LightTheme};

const DarkTheme = createTheme({
    palette:{
        type:"dark",
        primary:{
            main:"#303030"
        },
        secondary:{
            main:"#424242"
        }
    }
})
export default DarkTheme;
